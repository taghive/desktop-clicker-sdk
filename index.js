const init = require('./src/init');
const registerStudent = require('./src/register');
const listenEvent = require('./src/listenEvent');
const listenClickerEvent = listenEvent.listenClickerEvent;
const stopListening = listenEvent.stopListening;

module.exports = Object.assign({},{
    init,
    registerStudent,
    listenClickerEvent,
    stopListening
});
