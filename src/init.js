const isElevated = require('is-elevated');

const { app, ipcMain } = require('electron');
const SerialPort = require('serialport');

let Node = {
    child: require('child_process')
};

let MAX_BUFFER = 134217728;

//Initializing a dongle
function init( execName, callback) {
    app.on('ready', async () => {
        const gotTheLock = app.requestSingleInstanceLock();
        try {
            await sleep(500);
            if (!gotTheLock) {
                this.quit();
            } else {
                let isElevatedResult = await isElevated();
                if (isElevatedResult) {
                setupSerialPort();
                callback();
                } else {
                    var command = ['/usr/bin/pkexec', 'env', 'DISPLAY=$DISPLAY', 'XAUTHORITY=$XAUTHORITY'];
                    var magic = 'SUDOPROMPT';
                    command.push('/bin/bash -c "echo ' + magic.trim() +' ; ' + execName + '"');
                    //https://nodejs.org/api/child_process.html#child_process_child_process_spawn_command_args_options
                    command = command.join(' ');
                    Node.child.exec(command, { encoding: 'utf-8', maxBuffer: MAX_BUFFER }, (_error, _stdout, _stderr) => {
                        app.quit()
                    });
                }
            }
        } catch(e) {
            console.log(e);
        }
    });
}

function setupSerialPort() {
    ipcMain.on('serialport', (event, data) => {
      switch(data.type) {
          case 'open':
              SerialPort.list((error, ports) => {
                  if (error) {
                      event.sender.send('serialport', { type: 'error', payload: { message: error.message } });
                  } else {
                      for (var port of ports) {
                          console.log('port vendorId: ' + port.vendorId)
                          if (port.vendorId === '1915' && ((port.productId === '521a' || port.productId === '521A') || (port.productId === 'c00a' || port.productId === 'C00A'))) {
                              classKeySerialPort = new SerialPort(port.comName, {
                                  baudRate: 115200,
                                  databits: 8,
                                  parity: 'none'
                              }, false);

                              classKeySerialPort
                                  .on('open', () => {
                                      event.sender.send('serialport', { type: 'opened' });
                                  })
                                  .on('close', () => {
                                      console.log('close')
                                      event.sender.send('serialport', { type: 'closed' });
                                  })
                                  .on('error', (error) => {
                                      console.log('error')
                                      console.error(error);
                                      event.sender.send('serialport', { type: 'error', payload: { message: error.message } });
                                  })
                                  .on('data', (data) => {
                                      console.log("data received")
                                      console.log(data)
                                      console.log("data received finished")
                                      event.sender.send('serialport', { type: 'data', payload: data });
                                  });
                              return;
                          }
                      }
                      console.log('device not found')
                      event.sender.send('serialport', { type: 'error', payload: { message: 'device not found' } });
                  }
              });
              break;
          case 'close':
              if (classKeySerialPort && classKeySerialPort.isOpen) {
                  classKeySerialPort.close();
              }
              break;
          case 'write':
              if (classKeySerialPort && classKeySerialPort.isOpen) {
                  var writeBuffer = Buffer.from(data.payload);
                  classKeySerialPort.write(writeBuffer, function(err, result) {
                      if (err) {
                          console.log('Error while sending message : ' + err);
                      }
                      if (result) {
                          console.log('Response received after sending message : ' + result);
                      }
                  })
              }
              break;
      }
  });
};

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

module.exports = init;
